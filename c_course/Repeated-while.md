# While Loop 重複結構
在講結構的時候我們會說重複結構，Repeated Struture，而重複結構在C語言有兩個，一個是`for`，另一個是`while`。這篇我們先講`while`。

## While Loop
#### 程式碼
```c {3-7}
#include<stdio.h>
int main(){
    int count = 1;
	while (count <= 3){
	    printf("count 現在是 %d\n", count);
	    count = count + 1;
	}
}
```
 
#### 執行結果
```text
count 現在是 1
count 現在是 2
count 現在是 3
```
`第三行`我們設定一個變數`count`為`1`，然後說`while (count <= 3)`，也就是`當 count 小於等於 3 的時候`，我們執行這個程式區塊。

`第五、六行`我們先顯示了`count`當下的值，而後將它的值變成「原本再加一」。

`第六行`執行完畢之後，到達程式區塊的結尾，程式會回頭檢視`第四行`的條件是否成立，如果成立，則繼續執行該程式區塊，若不成立，則跳過該程式區塊。
而此例中，原本`count`為`1`，經過第一次執行後，變成`2`，仍然符合條件，因此又執行了一次`第五、六行`，不過此時從原本顯示為`1`變成顯示為`2`。

第二次執行完畢之後，再一次檢視條件，然後顯示`3`，並將`count + 1`變成`4`。此時就不符合條件，因此跳過程式區塊，程式結束。

這樣子一直反覆的執行同一個程式區塊，就是「重複結構」。而此重複結構是因為「特定條件成立」時執行，因此我們又稱它為「條件迴圈」。