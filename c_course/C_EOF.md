# EOF End Of File 使用

EOF 簡單來說就是輸入截止，在程式競賽很常用到，題目不會告訴你原先會有幾筆輸入，希望你可以自己判斷程式輸入完了。

## 範例

```c
#include<stdio.h>

int main() {
    int n;
    while(scanf("%d", &n) != EOF){
        printf("n = %d\n", n);
    }
}
```

## 練習題
- [a004](examples/a004)