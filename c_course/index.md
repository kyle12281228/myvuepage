# C 語言
本課程為基礎C語言課程，配合資訊必修課的進度及例子。這邊主要會紀錄重點，供大家課後複習查找資料方便，因此並不會很循序漸進講每個細節。

可以使用[Online GDB](https://www.onlinegdb.com)來線上測試程式。