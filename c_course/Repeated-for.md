# For Loop 重複結構
在[While Loop重複結構](Repeated-while)中，我們講到了條件迴圈 while loop 的使用方式，這篇我們要來講「計次迴圈」，for loop的使用方式。

## For Loop
這邊我們使用在 While Loop 的例子，你可以點下方「更改後的程式碼」來看改完之後的樣子。
#### 程式碼
```c 
#include<stdio.h>
int main(){
    int count = 1; // [!code --]
	while (count <= 3){ // [!code --]
	for(int count = 1; count <= 3; count = count + 1){ // [!code ++]
	    printf("count 現在是 %d\n", count);
	    count = count + 1; // [!code --]
	}
}
```
::: details 更改後的程式碼
```c {3-5}
#include<stdio.h>
int main(){
	for(int count = 1; count <= 3; count = count + 1){
	    printf("count 現在是 %d\n", count);
	}
}
```
:::
 
#### 執行結果
```text
count 現在是 1
count 現在是 2
count 現在是 3
```

此例中，我們把初始值`int count = 1;`、條件`count <= 3`、變量`count = count + 1`三件事情，都集中到`for`的小括號中。
這就是`for`的基礎用法，在括號中我們可以清楚地看到哪個變數在主導迴圈的進行，總共要進行幾次。此例中我們可以輕易看出`count`從`1`執行至`3`，總共三次。因此，我們稱它「計次迴圈」。

## 練習題
- [a005. Eva 的回家作業](examples/a005-eva)

