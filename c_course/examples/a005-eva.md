# a005. Eva 的回家作業
[ZeroJudge-a005](https://zerojudge.tw/ShowProblem?problemid=a005)
題目詳情請直接參考連結。
::: details C語言參考解答
```c
#include<stdio.h>
int main(){
    int count;
    scanf("%d", &count);
    for(int i = 1; i <= count; i++){
        int a1, a2, a3, a4, a5;
        scanf("%d %d %d %d", &a1, &a2, &a3, &a4);
        if (a2/a1 == a3/a2 && a4/a3 == a3/a2) {
            a5 = a4 * (a2 / a1);
        }
        else{
            a5 = a4 + (a2 - a1);
        }
        printf("%d %d %d %d %d\n", a1, a2, a3, a4, a5);        
    }

}
```
:::

## Index 搜尋關鍵字
for、等差、等比、數列、簡單、基礎