# a003. 兩光法師占卜術
[ZeroJudge-a003](https://zerojudge.tw/ShowProblem?problemid=a003)
題目詳情請直接參考連結。
::: details C語言參考解答
```c
#include<stdio.h>

int main() {
    int M;
    int D;
    scanf("%d %d", &M, &D);
    int S = (M * 2 + D) % 3;
    if (S == 0) printf("普通");
    else if (S == 1) printf("吉");
    else printf("大吉");
}
```
:::


::: details C++參考解答
```cpp
#include<bits/stdc++.h>
using namespace std;

int main() {
    int M;
    int D;
    cin >> M;
    cin >> D;
    int S = (M * 2 + D) % 3;
    if (S == 0) cout << "普通";
    else if (S == 1) cout << "吉";
    else cout << "大吉";
}
```
:::

## Index 搜尋關鍵字
基本輸入輸出、判斷、選擇結構、簡單、基礎、if