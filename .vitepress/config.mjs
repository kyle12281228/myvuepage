import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  markdown: {
    lineNumbers: true
  },
  lastUpdated: true,

  title: "冠曄的個人教學網站",
  description: "冠曄的個人網站，用來放各種東東",
  themeConfig: {
    search: {
      provider: 'local'
    },
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Python', link: '/python_course/' },
      { text: 'C/C++', link: '/c_course/' },
      { text: 'Binary World', link: 'https://binary-world.hat-and-cat.cc'},
      { text: 'Yeh Judge', link: 'https://zero.hat-and-cat.cc'},
    ],
    sidebar: {
      '/python_course/': [
        {
          text: 'Welcome', link: '/python_course/'
        },
        {
          text: '基礎語法',
          items: [
            { text: 'Hello World', link: '/python_course/basic/getting_started' },
            { text: '變數 Variable', link: '/python_course/basic/variable' },
            { text: '型態 Type', link: '/python_course/basic/type' },
            { text: '輸入與輸出 Input & Output', link: '/python_course/basic/io' },
            { text: '選擇結構 Selection', link: '/python_course/basic/selection' },
            { text: '函數 Function', link: '/python_course/basic/function' },
            { text: '重複結構 Repeated', link: '/python_course/basic/repeated' },
          ]
        },
        {
          text: '進階語法',
          items: [
            { text: 'Class 類別', link: 'python_course/advance/class' }
          ]
        },
        {
          text: '應用及專題',
          items: [
            { text: 'Line Notify', link: 'python_course/application/Line_notify' }
          ]
        },
      ],
      '/c_course/': [
        {
          text: 'C語言基礎',
          items: [
            {text: 'Input and Output 輸入與輸出', link: 'c_course/io'},
            {text: 'Selection 選擇結構', link: 'c_course/selection'},
            {text: 'While Loop 重複結構', link: 'c_course/Repeated-while'},
            {text: 'For Loop 重複結構', link: 'c_course/Repeated-for'},
            {text: 'EOF End Of File 使用', link: 'c_course/C_EOF'},
          ]
        },
        {
          text: 'C++',
          items: [
            {text: 'Split the String 字串分割', link: 'c_course/cpp/split'},
          ]
        },
        {
          text: '解題示例',
          items: [
            {text: 'a003. 兩光法師占卜術', link: 'c_course/examples/a003'},
            {text: 'a004. 文文的求婚', link: 'c_course/examples/a004'},
            {text: 'a005. Eva的回家作業', link: 'c_course/examples/a005-eva'},
            {text: 'a024. 最大公因數(GCD)', link: 'c_course/examples/a024'},
            {text: 'a040. 阿姆斯壯數', link: 'c_course/examples/a040'},
          ]
        }
      ],
    },

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/kyle12281228' },
      { icon: 'instagram', link: 'https://www.instagram.com/linyeh1228?igsh=M2xqMGprOXgwdDZs&utm_source=qr' },
      { icon: 'youtube', link: 'https://www.youtube.com/@meow-yeh/featured' },
    ]
  }
})
