# 變數 variable
###### tags: `python`
## Declear and assign a variable
試想如果今天我們要計算園遊會中，可樂賣了幾罐，可以這麼做：
```python=
# 可樂賣的數量
print("可樂賣了：", end='')
print(10, end = '')
print("罐")
# output: 可樂賣了10罐
```
如果過了幾分鐘，又多賣出5罐，可以寫成：
```python=
# 可樂賣的數量
print("可樂賣了：", end='')
print(10+5, end='')
print("罐")
# output: 可樂賣了15罐
```

雖然這樣可以進行記錄，但如果想要進行後續計算會顯得有點麻煩。
這時候我們可以使用一個「變數」將可樂的銷量給儲存起來：
```python=
# 可樂的數量
numberOfCoke = 10
print(numberOfCoke)
# output: 10
```
`numberOfCoke = 10`代表有一個變數叫做「`numberOfCoke`」，其值為10。
變數的命名基本上可以自己決定，這邊採用的叫做「小駝峰」命名法，也就是以小寫為開頭，並且遇到別的字的開頭改為大寫。採用有意義的命名可以讓你的程式易於維護。
（當然，命名是有限制的，在這裡不贅述。）

使用變數的好處多多，例如後面如果又賣了5罐，就可以這樣寫：
```python=
# 可樂的數量
numberOfCoke = 10
numberOfCoke = numberOfCoke + 5
print(numberOfCoke)
# output: 15
```
要注意這邊的「等於符號」跟一般數學上的意義稍有差別。
程式中的等於符號代表「assign（賦值、指派）」，也就是「將等號右邊的東西指派給等號左邊的變數」。
因此`numberOfCoke = numberOfCoke + 5`應看作：`numberOfCoke`原本為10，加了5之後得到15，重新指派給`numberOfCoke`這個變數，此時`numberOfCoke`就變成了15。

有了變數的概念，你可以做更多事。例如可樂每罐售價25，成本18元，你可以計算總收益。
```python=
numberOfCoke = 10
numberOfCoke = numberOfCoke + 5

priceOfCoke = 25
costOfCoke = 18

incomeOfCoke = numberOfCoke * (priceOfCoke - costOfCoke)

print(incomeOfCoke)
# output: 105
```
這時候你可能會想，那為什麼不要直接算`15 * (25-18)`就好，還要特地把他們變成變數然後再計算？
試想如果某天成本提高，成本變成20元，你想提高售價變成28元。此時應該怎麼修改程式？
當你看著上面的範例程式，很淺顯易懂的知道要修改`priceOfCoke`跟`costOfCoke`，但如果你當初只有紀錄數字，過了一陣子再回頭看，你記得自己在寫什麼嗎？

### Exercise
- 你進貨了25罐可樂，用一個變數來記錄它。如：`numberPurchaseOfCoke`
- 你想紀錄銷量，因此用一個變數紀錄賣的數量。如：`numberSoldOfCoke`
- 每罐可樂的成本是18元
- 每罐可樂售價25元。
- 你覺得數量不夠，又進貨了30罐。
- 園遊會結束，賣了40罐，請算出總收益。（總收益＝總成本-總收入）

## Wrapping up
- 變數可以使用「小駝峰」命名法。
- 命名最好有意義，可以幫助維護程式。
- 利用變數可以讓程式容易被理解。
- 利用變數可以讓重複的值容易修改。

## Little Project
想像你現在在規畫一個活動，或是開一間店。你需要估算成本、預計收益，你想將這些複雜的計算交給程式來做，而你需要的只是去變更某項數值，就可以得到計算結果。例如想觀察進貨100件跟進貨200件的差異。
- 為你的計算命名【我的XXX計算】，例如：我的貓咪咖啡店計算
- 列出可能的成本，把它變成變數、抽象化。記得進行適當的命名以增加閱讀性。
- 列出可能的收入，把它變成變數、抽象化。
- 利用`print`寫出你關心的資訊。

