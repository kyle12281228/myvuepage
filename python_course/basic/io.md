# 輸入與輸出
###### tags: `python`
## Format output

我們已經知道如何使用變數來紀錄數值，但如果要印出一句易於閱讀的結果，會需要很多步驟：
```python=
numberSoldOfCoke = 10
print("可樂賣了：", end='')
print(numberSoldOfCoke, end='')
print("罐")
# output: 可樂賣了：10罐
```
我們可以利用「格式化輸出」將這三行程式碼變成一行：
```python=
numberSoldOfCoke = 10
print("可樂賣了：%d罐" % (numberSoldOfCoke))
# output: 可樂賣了：10罐
```

`%d`代表的是「這個地方要替換成整數」的意思，而後面 `% (numberSoldOfCoke)`則是要替換成的東西。

繼續延伸這個例子：

```python
numberSoldOfCoke = 10
itemName = "可樂"
print("%s賣了：%d罐" % (itemName, numberSoldOfCoke))
# output: 可樂賣了：10罐

```


剛剛說`%d`代表整數，而現在多了一個`%s`代表字串。

後面的`% (itemName, numberSoldOfCoke)`代表依照順序替換成變數 `itemName` 、`numberSoldOfCoke`。

### Format Specifier


| 符號 | 意義 | 
| -------- | -------- | 
| %s     | 字串     |
| %d | 整數 |
| %f | 浮點數（小數）|

- 若在%跟英文中間加上整數，代表至少幾個字元的意思。預設靠右對齊，負整數則會靠左對齊。
例如：`%5d`代表輸出這個整數時，補齊至少五個字元。
- 若在浮點數`%f`加上小數點`.`，則代表保留小數幾位。
例如：`%.2f`代表輸出該小數時，保留小數點後兩位。

### Other ways
除了上述這個做法外，還有其他方法：
```python=
a = 123
print(f"a = {a:4d}")
# output: a =  123
print("a = {}".format(a))
# output: a = 123
```

## input()

當我們需要跟程式互動的時候，會想要讓使用者輸入東西，然後程式再根據使用者的輸入，做出相對應的輸出。這時候我們可以使用`input()`這個函數。

```python
a = input("please input an integer: ")
```

如此，當程式執行到這邊的時候，就會停下來，並且出現「`please input an integer: `」然後等待使用者輸入。

```python
a = input("please input an integer: ")
# 輸入8989339
print(a)
# output: '8989339'

print(a-3)
# Traceback (most recent call last):
#  File "<stdin>", line 1, in <module>
# TypeError: unsupported operand type(s) for -: 'str' and 'int'
```

這個地方會注意到一件事，我們明明輸入的是「整數」但輸出卻出現了引號，引號的出現代表它是一個「字串」。

因此當我們做數字的運算的時候，會出現錯誤！讓我們讀一下錯誤訊息：

`TypeError: unsupported operand type(s) for -: 'str' and 'int'`

代表它是一個型別錯誤（TypeError），他說`'str'`（字串）跟`'int'`（整數）不能做`-`減法的運算！

為什麼會這樣呢？Python將`input()`接收到的任何東西都當作字串，然後再讓程式設計者自行去決定怎麼處理它。

因此我們若要把它當作數字來處理，就要先進行型別的轉換！

這裡介紹兩個方法：

1. 轉型別

	```python
	a = '3'
	print(type(a))
	# output: <class 'str'>
	int(a)
	print(type(a))
	# output: <class 'int'>
	```

	這邊我們做的事情就是利用`int()`這個函數，強制將括號內的東西變成整數。

	但要注意的是，若變數a本身內容不是整數，那麼可能就會出錯！試試看令ａ是一個字串，然後轉型別看看吧！

2. `eval()`

	```python
	a = '3'
	eval(a)
	print(type(a))
	# output: <class 'int'>
	```

	`eval()`是一個好用的函數，可以直接理解成「將引號脫掉」。因此這邊若將第一行的a的引號脫掉，自然剩下3，是個整數。

	這裡比轉型別好用在哪呢？若是原本輸入是一個浮點數（小數），而妳強制更改型別成整數的話，那麼小數部分就會被捨去，而身為程式設計者的你可能渾然不知。但若用`eval()`，python可以自然而然幫你判斷成浮點數。

### Exercise 猜生日魔術
- 請一個人輸入自己的生日的月份，然後乘以4，等於A。
- 再加9，等於B。
- 再乘以25，等於C。
- 最後，再加上自己生日的日期，等於D。
- 然後，讓她把最後得出的數字D大聲的念出來。

此時，你偷偷把數字減掉225，就會是她的生日了！



## Wrapping up

你現在可以讓使用者輸入東西、或是自己宣告變數並且賦值，可以做基本的運算，可以印出東西。

## Little Project
來做一個【自我介紹生成器】吧！
- 找一個(或自己打一個)自我介紹模板
- 把關鍵詞變成變數
- 讓使用者在執行時輸入這些變數
- 除了直接輸入外，也可以有一些小變化，例如，輸入出生年，輸出年紀；輸入身高體重，輸出BMI。
- 可以將自我介紹變成其他東西，例如作文生成器等等。

### Advance
[園遊會規劃](https://colab.research.google.com/drive/1kCqzCJtbRm15DThpPnEfawU_4S3WUeL0?usp=sharing)

### 補充
#### 1. ```.split()```
當你需要單行設定多個變數時，可以用```.split()```，會把輸入的字串，根據（ ）內的值分割（ 其預設為 ```' '```空格 ），並依順序賦值於變數。
``` python 
a,b = input().split(',')
#輸入：1,2
print(f"a={a}")
print(f"b={b}")
#輸出1 2
```
而當你輸入多個數值進入 list 時，只要在外面再套一層```list()```，就可以根據```.split()```的設定分割值，把輸入的字串分割，並放入串列內。
``` python 
list123 = list(input().split())
#輸入 1 2 3 4 5
print(list123)
#輸出 ['1', '2', '3', '4', '5']
```