# Hello World
###### tags: `python`

>本教學除非另外說明，否則都是以[Colab](http://colab.research.google.com/)作為示範。您也可以使用[Online GDB](https://www.onlinegdb.com)進行練習。

>好用的Python學習網站：[W3School](https://www.w3schools.com/python/default.asp)、[SoloLearn](https://www.sololearn.com/home)

>由於 Replit 相關限制越來越多，因此這邊把連結拿掉了。

## Hello world
首先進入Colab，在框框裡面輸入`print("Hello World!")`，然後按播放鍵，這樣你的第一個程式就完成囉！
![](https://i.imgur.com/guOoB0v.png)

`print`就是「列印」的意思，在這邊，就是將括號裡面的字串給印出來。
`print`是一種「函數（式）」，就如同數學的`f(x)`那樣。輸入一個`x`，會有相對應的值出現。而此處，輸入的是一串字（稱作字串），輸出則是印出這串字。
試著將內容更換吧！也可以輸入中文哦！

## 換行
在很多程式語言中，換行會使用`\n`。當程式遇到反斜線`\`時，會將它視為某種指令，並不會直接將它印出來。而後面接的`n`代表「new line」，新的一行。所以`\n`就是在告訴`print`，此處要換行囉！
```python=
print("Hello\nWorld!")
# 輸出：
# Hello
# World!
```

`#`是Python作為「註解」的方式，當Python看到`#`的時候，不會執行它。

## Escape Character
除了「換行」，還有一些反斜線開頭的字元可以使用，我們將它稱作跳脫字元。
`\n` : 換行
`\t` : tab
`\\` : 斜線
`\"` : 雙引號
`\'` : 單引號
```python=
print("早餐店阿姨說\"帥哥，你的早餐好囉！\"")
# 早餐店阿姨說"帥哥，你的早餐好囉！"

print("早餐店阿姨說"帥哥，你的早餐好囉！"")
# 會報錯
```
在程式中，括號、引號等，通常都是「成雙成對」的。有左括號，就有右括號；出現一個雙引號，程式就會想找下一個雙引號在哪。
~~括號都有另一半了，我呢ＱＱ~~

```python=
print("Hello\tApple\nHello\tBanana\nHola\tAmingo")
"""
執行結果：
Hello	Apple
Hello	Banana
Hola	Amingo
"""
```

## end
在python中，`print`預設結尾自己加入換行`\n`，我們可以自訂這個結尾，例如：`print("some text ... ", end='')`
如此，結尾就會變成空的字串囉！
嘗試一下下列兩種寫法會產生什麼結果。
```python=
print("Hello, ")
print("world.")

print("Hello, ", end='')
print("world.", end='')
```

## sep
試著`print`下面兩種情形觀察他們的差異
```python
print('Hello a', 'Hello b')
```

```python
print('Hello a', 'Hello b', sep=',')
print('Hello a', 'Hello b', sep=',,,')
```

## Operator
運算符號在演算法設計上很常使用到，也就是小學學的「加減乘除」、括號、次方。還有兩個是取商數跟取餘數。
```python=
# 加法
print(1+1)
# output: 2

# 乘法
print(2*3) 
# output: 6

# 除法
print(3/2)
# output: 1.5

# 減法
print(3-2)
# output: 1

# 次方
print(3**2)
# output: 9

# 取商數
print(31//5)
# output: 6

# 取餘數
print(31%5)
# output: 1
```

「次方」的符號是兩次乘法符號，`3**2`代表3的2次方；

「取商數」意思是整除幾次的意思，例如 `31//5` 代表，31除以5的商數；

「取餘數」就是除完剩下的餘數，例如`31%5`代表31除以5的餘數（＝1）。

至於四則運算的規則跟一般都是一樣的，即「先乘除後加減」、「有括號先算」、「次方先算」，

若是不太確定運算順序，其實也可以直接將想先計算的部分括號起來，那麼必定會先算！

這裡要注意的是，跟剛剛印出Hello World不同，這次我沒有加上雙引號。

原因是因為，加入雙引號代表「字串」，而我們剛剛輸入的是數字運算，因此不用加引號。

但加引號並不會出錯！請試試看，若加入引號，輸出結果會有什麼不同？

### Exercise
```python=
試著利用 print，印出
「早餐店阿姨說"帥哥，你的餐點好囉"
明細：
品項    單價    總數    金額
蛋餅    25     25      ___
豆漿    15     30      ___
總共____圓。」<-- 金額直接用python幫你算～
# Hint : 使用不只一個print !
```

### Note
在Python 中，我們比較常使用單引號「`'字串'`」。
