# 型別 Type
###### tags: `python`
當我們宣告一個變數`purchaseOfCoke = 100`，把滑鼠放上去會看到![](https://i.imgur.com/FJyIrgi.png)
`int: purchaseOfCoke` 前面的`int`代表「integer（整數）」，也就是說，`purchaseOfCoke`是一個「整數型別的變數」。
你可以用`type()`看到某個變數是什麼型別：
```python=
purchaseOfCoke = 100
supplier = "Cokecola"
print(type(purchaseOfCoke))
print(type(supplier))
print(type(0.12))
# output:
# int
# str
# float
```
其中，`str`代表「string（字串）」、`float`代表「浮點數（小數）」。
python是「弱型別」的程式語言，簡言之，你不用特地告訴他你要用的變數是什麼型態，甚至你可以將一個變數從整數變成字串也是沒問題的。
（會這麼說是因為在滿多程式語言，這麼做都是會出錯的！）

那麼型別的意義在哪裡呢？
例如，你可能會討論一個整數的個十百千萬位，但不會討論一個字串的個十百千萬位。取而代之，你可能會想討論一個字串的長度、是否包含其他的字串。
例如，數字可以進行加減乘除的運算，但字串不行。

```python=
print(3*5)
# output: 15

print("abc" * 3)
# output: abcabcabc

print("abc" / 3)
# TypeError: unsupported operand type(s) for /: 'str' and 'int'
```
在python中，將字串進行乘法，會將該字串重複。但是如果進行除法，則會出現「TypeError」，代表型別上的錯誤。
`TypeError: unsupported operand type(s) for /: 'str' and 'int'`表示：不支援「/（除法）」在「字串」及「整數」間的運算。

### Try it
試著對整數、浮點數（小數）、字串進行不同的運算，觀察他的結果。如果出現錯誤，請試著解讀他的錯誤，想想為什麼會報錯。