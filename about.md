# 關於我 About me

## Introduction
我是林冠曄，現職國立鳳新高中資訊科技教師。這個網站首次建立於2024/1/14，目的是為了想把一些教學資源整合起來，供上課使用。

同時也歡迎大家使用，惟請注意引用時請註明來源，且若有商業使用需求，請另外與我聯絡。

I am Lin, currently employed as an Information Technology teacher at the National FengHsin Senior High School. This website was first established on January 14, 2024, with the purpose of integrating educational resources for classroom use.

Everyone is welcome to use the website, but please remember to cite the source when referencing the materials. For any commercial use requests, kindly contact me separately.

## Experience
- 2021 ~ now 國立鳳新高中資訊科技教師
- 2023 ~ now 高雄市教育局資訊教育中心 - Egame國中、高中課程委員

## My Works
- [二進位練習網站](https://binary.冠曄.tw)
- [Egame Python課程](/#)

## Contact
- Email: kyle1228@fhsh.khc.edu.tw
- Telephone: 07-765-8288 #5801