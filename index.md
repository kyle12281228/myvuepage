---
layout: home

hero:
  name: "冠曄的個人教學網站"
  text: "Meow"
  tagline: 我只是一隻貓咪🐱
  actions:
    - theme: brand
      text: About me
      link: /about
    - theme: alt
      text: Go to my Python Course
      link: /python_course/

features:
  - title: About
    details: 現職國立鳳新高中資訊科技專任教師
  - title: Contact
    details: kyle1228@fhsh.khc.edu.tw
---

